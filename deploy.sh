#!/bin/bash

echo -n "$GOOGLE_ENGINE_ID" > engine-id
echo -n "$GOOGLE_API_KEY" > api-key

kubectl create secret generic google-creds --from-file=./engine-id --from-file=./api-key
rm ./engine-id
rm ./api-key

kubectl apply -f k8s.yaml
