const express = require('express');
const GoogleImages = require('google-images');

const HOST = '0.0.0.0';
const PORT = 3000;
const QUERY_PREFIX = 'Armenian';

const POD_NAME = process.env.POD_NAME;
const POD_IP = process.env.POD_IP;
const ENGINE_ID = process.env.ENGINE_ID;
const API_KEY = process.env.API_KEY;

const app = express();
app.set('view engine', 'pug');
app.set('views', 'views');

const client = new GoogleImages(ENGINE_ID, API_KEY);

app.get('/', (req, res) => client.search(`${QUERY_PREFIX} ${req.query.query}`, { size: 'medium' })
	.then(images => images[Math.floor(Math.random() * images.length)])
	.then(randomImage => res.render('index', { description: randomImage.description, image: randomImage.url, podName: POD_NAME, podIp: POD_IP })));

app.listen(PORT, HOST);

console.log(`Listening on http://${HOST}:${PORT}`);
